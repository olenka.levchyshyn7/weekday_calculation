using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTest
{
    public class WorkDayCalculator : IWorkDayCalculator
    {
        public DateTime Calculate(DateTime startDate, int dayCount, WeekEnd[] weekEnds)
        {
            if (startDate == null)
                throw new ArgumentNullException();

            if (weekEnds == null)
                return startDate.AddDays(dayCount - 1);

            foreach (var weekend in weekEnds)
            {
                if (startDate > weekend.EndDate)
                    continue;
                System.TimeSpan diff = weekend.StartDate.Subtract(startDate);
                int weekend_num = weekend.EndDate.Subtract(weekend.StartDate).Days + 1;
                if (diff.Days < 0)
                {
                    startDate = weekend.EndDate.AddDays(1);
                    continue;
                }
                if (diff.Days < dayCount)
                    startDate = startDate.AddDays(diff.Days + weekend_num);
                else
                    startDate = startDate.AddDays(dayCount - 1);
                dayCount -= diff.Days;
            }
            if (dayCount != 0)
                startDate = startDate.AddDays(dayCount - 1);

            return startDate;
        }
    }
}
